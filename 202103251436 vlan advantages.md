Title: VLAN advantages

Tags: #post #vlan

Note:

A network without VLANs has some possible issues:

[[202103251438 possible issues of network without vlans]]

What are the advantages of using VLANs?

-   A VLAN is a single broadcast domain which means that if a user in the research VLAN would send a broadcast frame only users in the same VLAN will receive it.

-   Users are only able to communicate within the same VLAN unless you use a router.

-   Users don’t have to be grouped physically together. With VLANs, we can "logically" separate traffic.

Links:

https://networklessons.com/cisco/ccna-200-301/introduction-to-vlans