Title: VLAN delete on Cisco IOS

Tags: #cisco #ios #vlan 

Note:

## Delete all VLANs

You can delete all VLANs by deleting the vlan.dat file on your flash:

```
SW1#delete flash:vlan.dat
Delete filename [vlan.dat]? 
```

## Delete single VLAN:

You can delete a single VLAN like this:

```
SW1(config)#no vlan 456
```