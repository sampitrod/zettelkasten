Title: Add Interface To VLAN In Cisco IOS

Tags: #vlan #cisco #ios #configuration 

Note:

After creating a VLAN:

[[202103251445 create vlan on cisco ios]]

You can assign an interface to a VLAN like this:

```
SW1(config)interface fa0/1
SW1(config-if)#switchport mode access
SW1(config-if)#switchport access vlan 50
```

We can verify whether the interface is in the correct VLAN with this command:

```
SW1#show interfaces fa0/1 switchport
Name: Fa0/1
Switchport: Enabled
Administrative Mode: static access
Operational Mode: static access
Administrative Trunking Encapsulation: negotiate
Operational Trunking Encapsulation: native
Negotiation of Trunking: Off
Access Mode VLAN: 50 (Computers)
Trunking Native Mode VLAN: 1 (default)
```

Links: