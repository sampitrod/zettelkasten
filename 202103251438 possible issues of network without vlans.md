Title: Possible issues of network without VLANs

Tags: #vlan

Note:

Consider the picture below:

![[large-flat-network.png]]

In a switched network without VLANs, we might run into issues:

- Broadcast traffic is unrestricted and goes everywhere.
	- We can restrict broadcast traffic by using VLANs:
		- [[202103251431 vlan is separate broadcast domain]]
- Difficult to implement security. You could filter MAC addresses using something like port-security.

Links:

https://networklessons.com/cisco/ccna-200-301/introduction-to-vlans