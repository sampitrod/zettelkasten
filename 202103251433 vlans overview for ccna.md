Title: VLANs

Tags: #post #vlan

Note:

This note is an overview of everything there is to know about VLANs.

## Introduction

### VLANs

[[202103251436 vlan advantages]]

### Trunks

[[202103291352 trunk encapsulation types]]

## Configuration

### VLANs

[[202103251445 create vlan on cisco ios]]
[[202103251458 vlan delete on cisco ios]]
[[202103251502 add interface to vlan in cisco ios]]

### Trunks

[[202103251510 8021q trunk configuration on cisco ios]]

Links: